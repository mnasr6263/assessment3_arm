#!/bin/bash


cd Terraform/

terraform init

terraform plan -target="module.production_tier.aws_launch_template.pc_autoscaling_instances_template"

terraform destroy -target="module.production_tier.aws_launch_template.pc_autoscaling_instances_template" --auto-approve
