#!/bin/bash

echo "Building AMI with new changes..."

/usr/bin/packer init /home/ec2-user/sharedspace/terraformspace/Ansible/cfg.pkr.hcl

sed -i "s/arm-ami-packer-time/arm-ami-packer-$(date +%s)/g" /home/ec2-user/sharedspace/terraformspace/Ansible/provision_petclinic_ami.json

/usr/bin/packer build /home/ec2-user/sharedspace/terraformspace/Ansible/provision_petclinic_ami.json

