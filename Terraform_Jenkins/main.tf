/* -------------------------------------------------------------------------- */
/*                             Jenkins Environment                            */
/* -------------------------------------------------------------------------- */

provider "aws" {
    region = "eu-west-1"
}


/* ------------------------- Creating SuperNode and Workers ------------------------ */


# Launching supernode ami 
resource "aws_instance" "arm-jenkins-master1" {
  ami = data.aws_ami.master_latest_ami.id
  instance_type = "t2.medium"
  associate_public_ip_address = true
  subnet_id = data.aws_subnet.selected.id 
  security_groups = [aws_security_group.sg-super.id, aws_security_group.sg-workers.id]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = aws_key_pair.jenkins_keys.id
  tags = {
    "Name" = "arm-jenkins-master"
  }
}

# Launching two worker node instances through ami
resource "aws_instance" "arm-jenkins-worker1" {
  ami = "ami-081ab7a7625792b3c"
  instance_type = "t2.micro"
  associate_public_ip_address = true
  subnet_id = data.aws_subnet.selected.id 
  security_groups = [aws_security_group.sg-workers.id]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = aws_key_pair.jenkins_keys.id
  tags = {
    "Name" = "arm-jenkins-worker1"
  }
  
  user_data = templatefile(".//templates/jenkins_config.tpl", 
  {
    key_public = "${aws_key_pair.jenkins_keys.fingerprint}"
  })

}

resource "aws_instance" "arm-jenkins-worker2" {
  ami = "ami-081ab7a7625792b3c"
  instance_type = "t2.micro"
  associate_public_ip_address = true
  subnet_id = data.aws_subnet.selected.id 
  security_groups = [aws_security_group.sg-workers.id]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = aws_key_pair.jenkins_keys.id
  tags = {
    "Name" = "arm-jenkins-worker2"
  }

  user_data = templatefile(".//templates/jenkins_config.tpl", 
  {
    key_public = "${aws_key_pair.jenkins_keys.fingerprint}"
  }
  )
}

resource "aws_route53_record" "arm_jenkins_r53" {
  zone_id = "Z03386713MCJ0LHJCA6DL" # AL academy Zone ID
  name    = "arm-jenkins"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.arm-jenkins-master1.public_ip]
}

/* -------------------------------------------------------------------------- */
/*                           JENKINS SECURITY GROUPS                          */
/* -------------------------------------------------------------------------- */

/* ------------------------ SECURITY GROUP FOR MASTER ----------------------- */

resource "aws_security_group" "sg-super" {
    name = "arm-sg-master-super"
    description = "Security Group for Jenkins Master Node"
    vpc_id = data.aws_vpc.selected.id
    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [ "37.210.227.157/32", "87.80.216.64/32","185.59.124.94/32", "94.63.101.60/32"]
      description = "allows ssh to userids and from filipe"
    }
    ingress {
      from_port = 8080
      to_port = 8080
      protocol = "tcp"
      cidr_blocks = [ "13.52.5.96/28", "13.236.8.224/28", "18.136.214.96/28", "18.184.99.224/28", "18.234.32.224/28", "18.246.31.224/28", "52.215.192.224/28", "104.192.137.240/28", "104.192.138.240/28", "104.192.140.240/28", "104.192.142.240/28", "104.192.143.240/28", "185.166.143.240/28", "185.166.142.240/28"]
      description = "allows bb access for webhooks"
    }

    egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      description = "allows all ports"
    } 
  tags = {
    "Name" = "arm-sg-master-super"
  }
}

/* -------------------- SECURITY GROUP FOR JENKINS WORKER ------------------- */

resource "aws_security_group" "sg-workers" {
    name = "arm-sgg-jenkins"
    description = "Security Group for Jenkins Workers"
    vpc_id = data.aws_vpc.selected.id
    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [ "37.210.227.157/32", "87.80.216.64/32","185.59.124.94/32", "95.136.65.200/32", "94.63.101.60/32"]
      description = "allows ssh from userids and filipe "
    } 
         
    ingress {
      from_port = 8080
      to_port = 8080
      protocol = "tcp"
      cidr_blocks = [ "37.210.227.157/32", "87.80.216.64/32","185.59.124.94/32", "95.136.65.200/32", "94.63.101.60/32" ]
      description = "allows 8080 on userids and filipe"
    }

    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      self = true
      description = "allows ssh from jenkins machine"
    }

    egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      description = "allows all ports"
    } 
  tags = {
    "Name" = "arm-sgg-jenkins"
  }
}

/* --------------------- KEY PAIR FOR JENKINS ISNTANCES --------------------- */

resource "aws_key_pair" "jenkins_keys" {
  key_name   = "armkey_jenkins"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDsZ+685QGVY3UlDoSAQ+iEsT7gItoyyN0j3ZfivHj2JOSLfc2UdvlQ4Juk2nKplW4M2EgTLxF7jnWvZ95wrB6XqYj6zB/F566gnsDEsSfHwMUSJhwY4aIDwfc1YMxesyFIVmpk4QzsbwZ4A46hW9psdKFbD+22CLVxcZJt3F7nvDdcQEnFgjb0IDLTgB9t1NxPPmdOR9LjBelpBBPUzX7OKJwLxlrLkzP5CMFsUFdo9+4LBzR6Bnn/lKuG8lw9eEQtF5p3kPOlJ9zuJNPis+d0ybzgXniYwHqMSS2fMJOtQLGAmujX1RvJeS0xNPSymvLnqwvekEgWtdvwYZWYUGQD armkey"
}




resource "local_file" "jenkins_ip" {
    content  = templatefile(".//templates/jenkins_ip_info.tpl", 
  {
    master_pub_ip = "${aws_instance.arm-jenkins-master1.public_ip}"
    master_priv_ip = "${aws_instance.arm-jenkins-master1.private_ip}"
    worker1_pub_ip = "${aws_instance.arm-jenkins-worker1.public_ip}"
    worker1_priv_ip = "${aws_instance.arm-jenkins-worker1.private_ip}"
    worker2_pub_ip = "${aws_instance.arm-jenkins-worker2.public_ip}"
    worker2_priv_ip = "${aws_instance.arm-jenkins-worker2.private_ip}"
  })
    filename = "jenkins_ip.txt"
}

resource "local_file" "jenkins_ami" {
    content  = templatefile(".//templates/jenkins_ami_script.tpl", 
  {
    master_id = "${aws_instance.arm-jenkins-master1.id}"
  })
    filename = "create_jenkins_master_ami.sh"
}


data "aws_ami" "master_latest_ami" {
  most_recent      = true
  owners           = ["242392447408"]

  filter {
    name   = "tag:Project"
    values = ["arm-jenkins-master-ami"]
  }
}

data "aws_region" "current" {
}

data "aws_subnet" "selected" {
  id = "subnet-7bddfc1d"
}

data "aws_vpc" "selected" {
  id = "vpc-4bb64132"
}