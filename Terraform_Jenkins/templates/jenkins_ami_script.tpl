#!/bin/bash

AMI_ID=`sudo aws ec2 create-image \
    --instance-id ${master_id} \
    --name "arm-jenkins-master-ami-$(date +%s)" \
    --region eu-west-1 \
    --output text`


sudo aws ec2 create-tags \
    --resources $AMI_ID \
    --region eu-west-1 \
    --tags Key=Project,Value=arm-jenkins-master-ami

echo "AMI Created"

