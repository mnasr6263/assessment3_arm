# ARM PetClinic Documentation

## Contents
- [ARM PetClinic Documentation](#arm-petclinic-documentation)
  - [Contents](#contents)
  - [Useful Links](#useful-links)
  - [Introduction](#introduction)
  - [Summary of Infrastructure](#summary-of-infrastructure)
  - [Launching The Infrastructure](#launching-the-infrastructure)
  - [Modifying The Infrastructure](#modifying-the-infrastructure)
    - [Jenkins Jobs and Git Branches](#jenkins-jobs-and-git-branches)
    - [Modifying PetClinic Environment](#modifying-petclinic-environment)
    - [Modifying Jenkins Environment](#modifying-jenkins-environment)
    - [Testing Changes to PetClinic App](#testing-changes-to-petclinic-app)
  - [Accessing and Modifying Infrastructure Features](#accessing-and-modifying-infrastructure-features)
    - [AutoScaling Instances](#autoscaling-instances)
    - [RDS Backup and Restore](#rds-backup-and-restore)
    - [Password Rotation](#password-rotation)
      - [Accessing the Passwords](#accessing-the-passwords)
    - [Provisioning PetClinic](#provisioning-petclinic)
      - [Template files](#template-files)
    - [Bitbucket Webhooks](#bitbucket-webhooks)
    - [Slack Notification](#slack-notification)

## Useful Links

http://arm-jenkins.academy.labs.automationlogic.com:8080
  - Link to ARM Jenkins

http://arm-pc.academy.labs.automationlogic.com/
  - Link to ARM PetClinic

https://bitbucket.org/mnasr6263/assessment3_arm/src/master/
  - Link to ARM BitBucket Repo

## Introduction

This documentation provides an overview of the PetClinic infrastructure that will be launched through terraform. There will be an explanation of all the features addded, all the dependencies necessary, and all the commands needed to launch a fully functional PetClinic environment with a couple commands.

## Summary of Infrastructure

<img src="images/infrastructure.png"
     alt="Infrastructure to be created"
     width = "500" />

The diagram below provides an overview of the infrastructure that is created through Terraform. Terraform is a service that provides infrastructure as code resulting in the maintainability and modification of the infrastructure to be simple. 

Our full environment consists of two infrastructures both created by separate terraform files. The first environment is the Jenkins Environment, consisting of a master node and two worker nodes. The second environment is the PetClinic environment which consists of the networking modules including a VPC, security groups, NACL's, and subnets; and also the instances within them such as the loadbalancer, the autoscaling group, and the RDS database.

This setup will allow for an easy creation of the whole infrastructure with only a few commands. The files for creating the Jenkins environment is found in `Terraform_Jenkins` and the files for creating the PetClinic Environment is found in `Terraform`.

## Launching The Infrastructure 

This project focuses on producing simple, highly maintainable code which provides the most automation possible. This means the only manual set up that needs to be done to launch the entire environment is creating a fresh EC2 Instance through AWS. This ensures that this code is highly independent of dependencies as they are stored in AMIs.

1. First launch a Amazon Linux 2 AMI (HVM) instance. You must ensure that this isntance has roles which include policies which allow for EC2 and IAM role creation.
2. Copy and Paste the whole code block into your instance.

     ```sh
     sudo yum install -y yum-utils
     sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
     sudo yum -y install terraform
     sudo yum -y install git
     cd
     git clone https://ramana11235@bitbucket.org/mnasr6263/assessment3_arm.git
     cd /home/ec2-user/assessment3_arm/Terraform_Jenkins

     terraform init 
     terraform plan
     terraform apply --auto-approve
     cat jenkins_ip.txt

     (crontab -l 2>/dev/null; echo "55 1 * * * /home/ec2-user/assessment3_arm/Terraform_Jenkins/create_jenkins_master_ami.sh") | crontab -
     ```
3. This installs Terraform on the instance, clones this repo and creates the Jenkin's Environment (a Master node and two Worker nodes).
4. To then launch the entire PetClinic Environment, in your local repo, switch to branch `dev-production-tier` and simply make a push with changes. 
   - This works due to a predefined webhook between this branch in the repo and the Jenkins job (`terraform2-production-environment`) which triggers the creation of the PetClinic environment.
     - This is explained further later in the document.

These minimal commands and steps to launch the entire infrastructure ensure the code is highly resuable.

**Important**: The data regarding the unqiue infrastructure Terraform has created is within the terraform tfstate files. This means that the tfstate file for your PetClinic is in the Jenkins workspace within the Jenkins Master Node in the folder `/var/lib/jenkins/sharedspace/terraformspace/Terraform/`
   - The commands you ran in Step 2 also create a script which backs up the Jenkins Master where the tfstate is stored in the form of an AMI.
   - The last command of the block ensures the AMI backup command is run every night at 1:55 am.


## Modifying The Infrastructure

### Jenkins Jobs and Git Branches

In order to make any changes to the infrastructure, it is important to first be on the right git branch and understand the Jenkins job's which will implement these changes. This repo has three branches, one for each environment, these are: `dev-production-tier` for making changes to the PetClinic environment, `dev-testing-tier` to make changes to the testing environment, and `dev-jenkins-tier` to make safe changes to the Jenkins environment.

`dev-production-tier` is the branch you will be working in most for direct changes to the PetClinic environment. Making any changes in the `Terraform/` should be pushed through here.
A webhook triggered to the Jenkins job `terraform-production-environment` will implement any changes to the environment.

`dev-testing-tier` is the branch you will use when testing any changes to PetClinic code. The job order of the testing tier is explained below:
1. `testenv1-install-petclinic-dependencies`
   - This Jenkin job installs the dependencies required to run PetClinic. These dependencies are installed on node Worker1 which is treated as the test instance.
   - These dependencies are defined in the playbook in `/Ansible/playbook_install_petclinic_dependencies.yml` and can be changed if any additional dependencies are needed.
   
2. `testenv2-run-tests`
   - This job pull the PetClinic's developer's branch to test the changes in the new code.
   - If these tests fail, a Slack notification is sent notifying the developers of the failure of the job.
   - If the tests pass, the new code is pushed to the PetClinic's developer's main branch and the job `testenv3-packer-successful-test` is triggered. 
3. `testenv3-packer-successful-test`
   - This job triggers a Packer build which provisions an AMI with the PetClinic dependencies and the new changes installed.
   - The playbook which provisions the new AMI is `playbook_petclinic.yml`
   - Once a new AMI has been provisioned and is ready, a Slack notification is then sent to the developers who can then implement the AMI into the production PetClinic environment by triggering the next job.
   - Up till this job, all the jobs would have been triggered automatically from job `testenv1-install-petclinic-dependencies`
4. `testenv4-refresh-webservers`
   - This job then refreshes the PetClinic webservers and updates them with the new AMI.

As `dev-jenkins-tier` is not connected to any webhooks, it will be explained separately.

### Modifying PetClinic Environment

Making changes to the PetClinic production environment is relatively simple. This is because, by design, the entire infrastructure sits in `/Terraform/modules/production_tier/main.tf`. Any changes to the network (VPC, subnets, security groups) or to instances (auto scaling group, loadbalancer, bastion) can be easily made in that file. 

To make changes:

1. Ensure you are in the branch `dev-production-tier`
2. Make necessary changes to `/Terraform/modules/production_tier/main.tf`
3. Push changes to the remote repo.
   - This triggers the job `terraform-production-environment` and whether the changes are able to be implemented without errors would be notified through Slack.
   - If your changes produced errors, continue making changes in the branch until your changes are successful.
4. Push the succesfull chanegs onto `master` branch.
   - This ensures `master` branch always has fully working code.

### Modifying Jenkins Environment 

Before making changes to the Jenkins environment, it is important to run the script `create_jenkins_master_ami.sh`. This creates a unique, latest AMI of the current Jenkins Master node. While this script is run every night at 1:55 am, it is important to run it before making changes to the Jenkins environment as it will backup and save the latest tfsate for the PetClinic environment.

1. SSH into the fresh instance you used to create Jenkins environment.
2. Run `create_jenkins_master_ami.sh`
3. On branch `dev-jenkins-tier`, make your changes to `/Terraform_Jenkins/main.tf` on your local repo and push them to the remote repo.
4. On your instance run command `git pull origin dev-jenkins-tier` to get the latest code.
5. Run `terraform init`, `terraform apply` to apply your changes.
6. If your changes are successful, push your code to `master` branch. 

### Testing Changes to PetClinic App
These tests are automated and the results of them would be sent by a Slack notification to you. To trigger these tests simply switch to `dev-testing-tier` and push to that branch to trigger the Jenkins job `testenv1-install-petclinic-dependencies`. Given the jobs that trigger after this, as mentioned, the results of the changes to the PetClinic would be sent as a Slack messsage.

And, as mentioned, to implement a successful change to the production tier, simply run the job `testenv4-refresh-webservers`.

## Accessing and Modifying Infrastructure Features

### AutoScaling Instances

Arguably the most important feature of this project is the AutoScaling instances. In order to compensate to any increased traffic where the web servers struggle to run, Amazon's auto scaling feature will launch new instance and automatically attach them to the loadbalancer.

As the CPU utilisation of instances reach the threshold, as mentioned in the autoscaling policy, the autoscaling group will delete that instance and create a new one so there are always three 'healthy' instances.

The policies which trigger the scaling can be found and changed in `/Terraform/modules/production_tier/main.tf` in the section `AUTOSCALING GROUP AND ATTACHMENT`.

### RDS Backup and Restore

The RDS database will have a mantainance window of Friday 3:00 till 3:30am. Snapshots will also be created everyweek so all the data is backed up.

To restore a database snapshot:

1. Log-on to Amazon's console and click on "RDS" 
2. Click "Automated backups"
3. Click on "arm-pc-database"
4. Click on "Actions"
5. CLick on "restore to point in time"

### Password Rotation

In order to trigger the resource module `random_password` which creates a random password in the form of a sensitive string, everyday at 3am the job `testenv4-refresh-webservers` is first run to destroy the current PetClinic webservers. Upon completion, the job `terraform-production-environment` is then run to recreate the destroyed webservers with an updated AMI (if there are any). This job also triggers a refresh of the resource module `random_password` wherein a new password is also created for the database, the provisioning scripts for the bastion and webservers are updated accordingly and PetClinic continues running normally. The current downtime during this process would be ~5 min at 3am. This is a relatively low downtime at a low traffic hour.

#### Accessing the Passwords

The webservers are updated and provisioned accordingly through the `user_data` automatically. This means the passwords do not need to be manually updated or even known. However for the case where a database connection might be needed to made through another platform for example, the password can be viewed on either the bastion or webserver instances at `/tmp/db_config.cnf`. 

### Provisioning PetClinic

While the dependencies for the PetClinic app are already installed at the AMI stage, they must be updated with the latest database data to login and retrieve data. This is done through `user_data` scripts which are run on creation of the instances. These scripts are custom made through template files.

####  Template files 
The template files located in `/Terraform/templates/` and `/Terraform_Jenkins/templates/` allows the configuration of user data of your webservers and bastion server using variables. Using the username, password and host defined in the `main.tf` file. 

Likewise, the template files in the Terraform_Jenkins directory allows you to create a bashscript that is rendered with the instance ID of the Jenkins master that, if run, will create an AMI of the master.  

### Bitbucket Webhooks

The master branch will always contain the latest version of working/deployable code.

Each developer has their own branch. Before editing any code on their own branch they must pull from master, so that the latest version of code is on their branch. 

If a change needs to be done the developer can merge there code on the dev-production-tier branch.

This will then trigger the jenkins pipeline which would terraform plan and terraform apply the code. Once the code is tested and successful then the developers must push their working code onto the master branch.

These webhooks are configured through the DNS that is created for the Jenkins master through Route53, this means that the webhook does not need to be changed if ever the Jenkins environments get changed. The only time the webhook configuration would have to be changed is when the DNS gets changed.

### Slack Notification
After every Jenkins job the developer will receive a message on slack with an update on the job. There will be two possible responses, the job successfully run or the job failed to run. 

To achieve this feature we used a slack plugin on Jenkins and installed Jenkins on slack. 

Developers can also customise messages very conveniently using this Jenkins plug-in. Scroll down to post-build actions and click on slack and then click on custom messages. 

All messages will be sent to
``` Jenkins-alert-arm ```

## Miscellaneous Notes

Please note that the security groups are such that only the Bastion is allowed SSH access on port 22 to any webservers. This is for increased security so please ensure that `armkey.pem` is on the bastion instance to SSH into.