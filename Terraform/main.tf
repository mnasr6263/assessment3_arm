
# /* -------------------------------------------------------------------------- */
# /*                                  PROVIDER                                  */
# /* -------------------------------------------------------------------------- */

provider "aws" {
    region = "eu-west-1"
}

/* -------------------------------------------------------------------------- */
/*                     LAUNCHING PRODUCTION INFRASTRUCTURE                    */
/* -------------------------------------------------------------------------- */
module "production_tier" {
    source = ".//modules/production_tier"
}

