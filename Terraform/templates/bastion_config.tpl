#!/bin/bash

## DB Config File
echo "[client]
user=${username}
password=${password}
host=${host}
port=3306" >/tmp/db_config.cnf

echo "# database init, supports mysql too
database=mysql
spring.datasource.url=jdbc:mysql://${host}/petclinic
spring.datasource.username=${username}
spring.datasource.password=${password}

# spring.datasource.initialize=true
#spring.datasource.schema=classpath*:db/{database}/schema.sql
#spring.datasource.data=classpath*:db/{database}/data.sql

# Web
spring.thymeleaf.mode=HTML

# JPA
spring.jpa.hibernate.ddl-auto=none

# Internationalization
spring.messages.basename=messages/messages

# Actuator / Management
management.endpoints.web.base-path=/manage
# Spring Boot 1.5 makes actuator secure by default
management.endpoints.web.enabled=true

# Logging
logging.level.org.springframework=INFO
# logging.level.org.springframework.web=DEBUG
# logging.level.org.springframework.context.annotation=TRACE

# Active Spring profiles
spring.profiles.active=production" >/home/ec2-user/petclinic/src/main/resources/application.properties


## Commands to populate DB 
sudo mysql --defaults-file=/tmp/db_config.cnf --execute="source /home/ec2-user/petclinic/src/main/resources/db/mysql/schema.sql;"
sudo mysql --defaults-file=/tmp/db_config.cnf --execute="source /home/ec2-user/petclinic/src/main/resources/db/mysql/data.sql;"
sudo mysql --defaults-file=/tmp/db_config.cnf --execute="CREATE USER 'petclinic'@'%' IDENTIFIED BY 'petclinic';" --database=petclinic
sudo mysql --defaults-file=/tmp/db_config.cnf --execute="GRANT ALL PRIVILEGES ON petclinic.* TO 'petclinic'@'%';"--database=petclinic