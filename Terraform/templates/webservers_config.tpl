#!/bin/bash

## DB Config File
echo "[client]
user=${username}
password=${password}
host=${host}
port=3306" >/tmp/db_config.cnf

echo "# database init, supports mysql too
database=mysql
spring.datasource.url=jdbc:mysql://${host}/petclinic
spring.datasource.username=${username}
spring.datasource.password=${password}

# spring.datasource.initialize=true
#spring.datasource.schema=classpath*:db/{database}/schema.sql
#spring.datasource.data=classpath*:db/{database}/data.sql

# Web
spring.thymeleaf.mode=HTML

# JPA
spring.jpa.hibernate.ddl-auto=none

# Internationalization
spring.messages.basename=messages/messages

# Actuator / Management
management.endpoints.web.base-path=/manage
# Spring Boot 1.5 makes actuator secure by default
management.endpoints.web.enabled=true

# Logging
logging.level.org.springframework=INFO
# logging.level.org.springframework.web=DEBUG
# logging.level.org.springframework.context.annotation=TRACE

# Active Spring profiles
spring.profiles.active=production" >/home/ec2-user/petclinic/src/main/resources/application.properties


sudo kill -9 $(ps -fC java | awk '{print $2}')
sudo env "PATH=$PATH" mvn -f /home/ec2-user/petclinic/ -Dmaven.test.skip=true package
sudo env "PATH=$PATH" nohup java -jar /home/ec2-user/petclinic/target/*.jar >/dev/null 2>&1 &