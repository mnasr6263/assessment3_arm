/* -------------------------------------------------------------------------- */
/*                          NETWORKING INFRASTRUCTURE                         */
/* -------------------------------------------------------------------------- */

/* ----------------------------- SECURITY GROUPS ---------------------------- */

/* ----------------- Security Group for Petclinic Webservers ---------------- */

resource "aws_security_group" "sg-pc" {
    name = "arm-SG-pc-webservers"
    description = "sg-for-arm-pc"
    vpc_id = aws_vpc.arm-vpc.id
    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [ "37.210.227.157/32", "87.80.216.64/32","185.59.124.94/32" ]
      description = "allows ssh from userids"
    }
    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      security_groups = [aws_security_group.sg-bastion.id]
      description = "allows ssh from bastion"
    }    
    ingress {
      from_port = 8080
      to_port = 8080
      protocol = "tcp"
      security_groups = [ aws_security_group.sg-lb.id ]
      description = "allows port 8080 from loadbalancer"
    }
    ingress {
      from_port = 8080
      to_port = 8080
      protocol = "tcp"
      cidr_blocks = [ "37.210.227.157/32", "87.80.216.64/32","185.59.124.94/32" ]
      description = "allows port 8080 from home ip for test"
    }    

    egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  tags = {
    "Name" = "arm-SG-pc-webservers"
  }
}

/* --------------------- Security Group for Loadbalaner --------------------- */

resource "aws_security_group" "sg-lb" {
    name = "arm-SG-loadbalancer"
    description = "sg-for-arm-lb"
    vpc_id = aws_vpc.arm-vpc.id
    ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = [ "0.0.0.0/0"]
      description = "allows all traffic to 8080"
    }
    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [ "37.210.227.157/32", "87.80.216.64/32","185.59.124.94/32" ]
      description = "allows ssh to userids"
    }

    egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    } 
  tags = {
    "Name" = "arm-SG-loadbalancer"
  }
}

/* ---------------------- Security Group for Super Node --------------------- */

resource "aws_security_group" "sg-super" {
    name = "arm-SG-super"
    description = "sg-for-arm-lb"
    vpc_id = aws_vpc.arm-vpc.id
    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [ "37.210.227.157/32", "87.80.216.64/32","185.59.124.94/32" ]
      description = "allows ssh to userids"
    }
    egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      description = "allows all ports"
    } 
  tags = {
    "Name" = "arm-SG-super"
  }
}


/* ----------------------- Security Group for Database ---------------------- */

resource "aws_security_group" "sg-rds" {
    name = "arm-SG-rds"
    description = "sg-for-arm-rds"
    vpc_id = aws_vpc.arm-vpc.id
    ingress {
      from_port = 3306
      to_port = 3306
      protocol = "tcp"
      security_groups = [aws_security_group.sg-pc.id, aws_security_group.sg-bastion.id]
      description = "allows 3306 traffic from petclinic webservers"
    }
    egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    } 

      tags = {
    "Name" = "arm-SG-rds"
  }
}

/* ----------------------- Security Group for Bastion ----------------------- */

resource "aws_security_group" "sg-bastion" {
    name = "arm-sg-bastion"
    description = "sg for bastion"
    vpc_id = aws_vpc.arm-vpc.id
    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [ "37.210.227.157/32", "87.80.216.64/32","185.59.124.94/32" ]
      description = "allows ssh to userids test"
    }
    egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      description = "allows all ports"
    } 
  tags = {
    "Name" = "arm-sg-bastion"
  }
}


/* ----------------------------------- VPC ---------------------------------- */

#Create a VPC
resource "aws_vpc" "arm-vpc" {
    cidr_block = "90.133.0.0/16"
    enable_dns_support = "true" 
    enable_dns_hostnames = "true"
    enable_classiclink = "false"
    instance_tenancy = "default"    
  tags = {
        Name = "arm-vpc"
    }
}

/* --------------------------------- SUBNETS -------------------------------- */


#Create a Public subnet in 1a
resource "aws_subnet" "arm-subnet-public-1a" {
  vpc_id = aws_vpc.arm-vpc.id
  cidr_block = "90.133.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1a"
  tags = {
    "Type" = "Public"
    "Name" = "arm-subnet-public-1a"
  }
}
#Create a private subnet in 1a
resource "aws_subnet" "arm-subnet-private-1a" {
  vpc_id = aws_vpc.arm-vpc.id
  cidr_block = "90.133.99.0/24"
  availability_zone = "eu-west-1a"
  tags = {
    "Type" = "Private"
    "Name" = "arm-subnet-private-1a"
  }
}

#Create a public subnet in 1b
resource "aws_subnet" "arm-subnet-public-1b" {
  vpc_id = aws_vpc.arm-vpc.id
  cidr_block = "90.133.2.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1b"
  tags = {
    "Type" = "Public"
    "Name" = "arm-subnet-public-1b"
  }
}
#Create a private subnet in 1b
resource "aws_subnet" "arm-subnet-private-1b" {
  vpc_id = aws_vpc.arm-vpc.id
  cidr_block = "90.133.100.0/24"
  availability_zone = "eu-west-1b"
  tags = {
    "Type" = "Private"
    "Name" = "arm-subnet-private-1b"
  }
}


#Create a public subnet in 1c
resource "aws_subnet" "arm-subnet-public-1c" {
  vpc_id = aws_vpc.arm-vpc.id
  cidr_block = "90.133.3.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1c"
  tags = {
    "Type" = "Public"
    "Name" = "arm-subnet-public-1c"
  }
}
#Create a private subnet in 1c
resource "aws_subnet" "arm-subnet-private-1c" {
  vpc_id = aws_vpc.arm-vpc.id
  cidr_block = "90.133.101.0/24"
  availability_zone = "eu-west-1c"
  tags = {
    "Type" = "Private"
    "Name" = "arm-subnet-private-1c"
  }
}

/* ----------------------------------- IGW ---------------------------------- */

#Create an IGW
resource "aws_internet_gateway" "arm-igw" {
  vpc_id = "${aws_vpc.arm-vpc.id}" 
  tags = {
    "Name" = "arm-igw"
  }
}

/* ------------------------------- ROUTE TABLE ------------------------------ */

# Creating Public Route Table
resource "aws_route_table" "rtb_public" {
  vpc_id = "${aws_vpc.arm-vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.arm-igw.id}"
  }
  tags = {
    "Name" = "arm-public-route-table"
  }
}

## Associating Public Route Tables
resource "aws_route_table_association" "arm-rta" {
  subnet_id = "${aws_subnet.arm-subnet-public-1a.id}"
  route_table_id = "${aws_route_table.rtb_public.id}"
}
resource "aws_route_table_association" "arm-rta-1b" {
  subnet_id = "${aws_subnet.arm-subnet-public-1b.id}"
  route_table_id = "${aws_route_table.rtb_public.id}"
}

resource "aws_route_table_association" "arm-rta-1c" {
  subnet_id = "${aws_subnet.arm-subnet-public-1c.id}"
  route_table_id = "${aws_route_table.rtb_public.id}"
}

# Creating private route table
resource "aws_route_table" "rtb_private" {
  vpc_id = "${aws_vpc.arm-vpc.id}"
  tags = {
    "Name" = "arm-private-route-table"
  }
}

## Associating Private Route Tables
resource "aws_route_table_association" "arm-rta-private" {
  subnet_id = "${aws_subnet.arm-subnet-private-1a.id}"
  route_table_id = "${aws_route_table.rtb_private.id}"
}
resource "aws_route_table_association" "arm-rta-1b-private" {
  subnet_id = "${aws_subnet.arm-subnet-private-1b.id}"
  route_table_id = "${aws_route_table.rtb_private.id}"
}

resource "aws_route_table_association" "arm-rta-1c-private" {
  subnet_id = "${aws_subnet.arm-subnet-private-1c.id}"
  route_table_id = "${aws_route_table.rtb_private.id}"
}


/* ---------------------------------- NACL ---------------------------------- */

#Create a Public NACL
resource "aws_network_acl" "arm-public-nacl" {
  vpc_id = aws_vpc.arm-vpc.id
  subnet_ids = [ aws_subnet.arm-subnet-public-1a.id, aws_subnet.arm-subnet-public-1b.id, aws_subnet.arm-subnet-public-1c.id ]
  egress {
    action = "allow"
    cidr_block = "0.0.0.0/0"
    from_port = 0
    icmp_code = 0
    icmp_type = 0
    protocol = "-1"
    rule_no = 100
    to_port = 0
  } 
  ingress {
    action = "allow"
    cidr_block = "0.0.0.0/0"
    from_port = 0
    icmp_code = 0
    icmp_type = 0
    protocol = "-1"
    rule_no = 100
    to_port = 0
  }
 tags = {
   "Name" = "arm-public-nacl"
 }
}

#Create a private nacl 
resource "aws_network_acl" "arm-private-nacl" {
  vpc_id = aws_vpc.arm-vpc.id
  subnet_ids = [ aws_subnet.arm-subnet-private-1a.id, aws_subnet.arm-subnet-private-1b.id, aws_subnet.arm-subnet-private-1c.id  ]

  egress {
    action = "allow"
    cidr_block = "0.0.0.0/0"
    from_port = 0
    protocol = "-1"
    rule_no = 100
    to_port = 0
  } 

  ingress {
    action = "allow"
    cidr_block = aws_vpc.arm-vpc.cidr_block
    from_port = 0
    protocol = "-1"
    rule_no = 100
    to_port = 0
  }

 tags = {
   "Name" = "arm-private-nacl"
 }
}



/* -------------------------- KEYPAIR FOR INSTANCES -------------------------- */

resource "aws_key_pair" "pc_as_keys" {
  key_name   = "armkey2"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDsZ+685QGVY3UlDoSAQ+iEsT7gItoyyN0j3ZfivHj2JOSLfc2UdvlQ4Juk2nKplW4M2EgTLxF7jnWvZ95wrB6XqYj6zB/F566gnsDEsSfHwMUSJhwY4aIDwfc1YMxesyFIVmpk4QzsbwZ4A46hW9psdKFbD+22CLVxcZJt3F7nvDdcQEnFgjb0IDLTgB9t1NxPPmdOR9LjBelpBBPUzX7OKJwLxlrLkzP5CMFsUFdo9+4LBzR6Bnn/lKuG8lw9eEQtF5p3kPOlJ9zuJNPis+d0ybzgXniYwHqMSS2fMJOtQLGAmujX1RvJeS0xNPSymvLnqwvekEgWtdvwYZWYUGQD armkey"
}


/* -------------------------------------------------------------------------- */
/*                      LAUNCHING AUTO SCALING INSTANCES                      */
/* -------------------------------------------------------------------------- */

/* ----------------------------- Launch Template ---------------------------- */

resource "aws_launch_template" "pc_autoscaling_instances_template" {
  name   = "pc_autoscaling_instances"
  image_id      = data.aws_ami.petclinic_latest_ami.id
  instance_type = "t2.micro"
  key_name = aws_key_pair.pc_as_keys.id
  vpc_security_group_ids = [aws_security_group.sg-pc.id]
  update_default_version = true
  monitoring {
    enabled = true
  }
  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "arm-pc-as-webserver"
    }
  }
  ## Feed it the launch template
  user_data = base64encode(templatefile(".//templates/webservers_config.tpl", 
  {
    username = "${aws_db_instance.arm_pc_db.username}"
    password = "${aws_db_instance.arm_pc_db.password}"
    host = "${aws_db_instance.arm_pc_db.address}"
  }
  ))

}

/* -------------------- AUTOSCALING GROUP AND ATTACHMENT -------------------- */

resource "aws_autoscaling_group" "pc_autoscaling_instances_group" {
  name               = "pc_autoscaling_group"
  max_size           = 5
  min_size           = 3
  vpc_zone_identifier = [aws_subnet.arm-subnet-public-1a.id, aws_subnet.arm-subnet-public-1b.id, aws_subnet.arm-subnet-public-1c.id]
  health_check_type         = "ELB"


  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]

  metrics_granularity = "1Minute"

  
  launch_template {
    id      = aws_launch_template.pc_autoscaling_instances_template.id
    version = "$Default"
  }
  lifecycle {
    ignore_changes = [load_balancers, target_group_arns]
  }
  tag {
    key                 = "Name"
    value               = "arm-pc-as-webservers"
    propagate_at_launch = true
  }

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 0
    }
  }

}
resource "aws_autoscaling_attachment" "pc_autoscaling_attachment" {
  autoscaling_group_name = aws_autoscaling_group.pc_autoscaling_instances_group.id
  alb_target_group_arn   = aws_lb_target_group.pc_loadbalancer_target_groups.arn
}

resource "aws_autoscaling_policy" "arm_policy" {
  name                   = "arm_policy"
  autoscaling_group_name = aws_autoscaling_group.pc_autoscaling_instances_group.name
  policy_type            = "TargetTrackingScaling"


  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 40.0
  }
}

/* -------------------------------------------------------------------------- */
/*                          APPLICATION LOADBALANCER                          */
/* -------------------------------------------------------------------------- */



/* ------------------------------ LOAD BALANCER ----------------------------- */

resource "aws_lb" "pc_loadbalancer" {
  name               = "arm-petclinic"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.sg-lb.id]
  subnets            = [aws_subnet.arm-subnet-public-1a.id, aws_subnet.arm-subnet-public-1b.id, aws_subnet.arm-subnet-public-1c.id]
  enable_deletion_protection = false


  tags = {
    Environment = "production"
  }
}

/* ----------------------- LOAD BALANCER TARGET GROUP ----------------------- */

resource "aws_lb_target_group" "pc_loadbalancer_target_groups" {
  name     = "arm-lb-target-groups"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = aws_vpc.arm-vpc.id
  slow_start = 160
  target_type = "instance"
  health_check {
    timeout = "5"
    interval = "10"    
  }

  depends_on = [
    aws_autoscaling_group.pc_autoscaling_instances_group
  ]

}  

/* ------------------------- LOAD BALANCER LISTENER ------------------------- */

resource "aws_lb_listener" "pc_front_end" {
  load_balancer_arn = aws_lb.pc_loadbalancer.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.pc_loadbalancer_target_groups.arn
  }
}


/* --------------------------- ROUTE 53 PETCLINIC --------------------------- */

resource "aws_route53_record" "pc_r53" {
  zone_id = "Z03386713MCJ0LHJCA6DL" # AL academy Zone ID
  name    = "arm-pc"
  type    = "A"
  alias {
    name                   = aws_lb.pc_loadbalancer.dns_name
    zone_id                = aws_lb.pc_loadbalancer.zone_id
    evaluate_target_health = true
  }
}

/* -------------------------------------------------------------------------- */
/*                          CREATING RDS DB INSTANCE                          */
/* -------------------------------------------------------------------------- */
 resource "aws_db_instance" "arm_pc_db" {
   identifier           = "arm-pc-database"
   allocated_storage    = 10
   engine               = "mysql"
   engine_version       = "5.7"
   instance_class       = "db.t3.micro"
   username             = "admin"
   password             = random_password.db_master_pass.result
   parameter_group_name = "default.mysql5.7"
   skip_final_snapshot  = true
   db_subnet_group_name = aws_db_subnet_group.arm_pc_db_subnet.id
   vpc_security_group_ids = [aws_security_group.sg-rds.id]
   maintenance_window      = "Fri:03:00-Fri:03:30"
   backup_retention_period = 7 #how long backup is kept
 }

/* ------------------------------- DB INSTANCE ------------------------------ */



/* ----------------------------- DB SUBNET GROUP ---------------------------- */

resource "aws_db_subnet_group" "arm_pc_db_subnet" {
  name = "arm_db_subnet"
  subnet_ids = [aws_subnet.arm-subnet-private-1a.id, aws_subnet.arm-subnet-private-1b.id, aws_subnet.arm-subnet-private-1c.id]
  tags = {
    Name = "My test DB subnet group"
  }
}

/* -------------------------- DB PASSWORD CREATION -------------------------- */


 resource "random_password" "db_master_pass" {
   length           = 16
   special          = false
 }


/* ------------------- CREATING BASTION FOR DB POPULATION ------------------- */


resource "aws_instance" "arm-pc-bastion" {
  ami = data.aws_ami.petclinic_latest_ami.id
  instance_type = "t2.micro"
  associate_public_ip_address = true
  subnet_id = aws_subnet.arm-subnet-public-1a.id
  security_groups = [aws_security_group.sg-bastion.id]
  key_name = aws_key_pair.pc_as_keys.id
  tags = {
    "Name" = "arm-pc-bastion"
  }

  user_data = templatefile(".//templates/bastion_config.tpl", 
  {
    username = "${aws_db_instance.arm_pc_db.username}"
    password = "${aws_db_instance.arm_pc_db.password}"
    host = "${aws_db_instance.arm_pc_db.address}"
  }
  )
}

/* -------------- DATA SOURCE TO FIND LATEST ARM PETCLINIC AMI -------------- */

data "aws_ami" "petclinic_latest_ami" {
  most_recent      = true
  owners           = ["242392447408"]

  filter {
    name   = "tag:Project"
    values = ["arm-pc-ami-packer"]
  }
}


